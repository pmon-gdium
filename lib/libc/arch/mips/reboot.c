void
tgt_reboot()
{

	void (*longreach) (void);

	int tmp;
	
	tmp = *(volatile unsigned long *)(0xbfe00100 + 0x20);
	*(volatile unsigned long *)(0xbfe00100 + 0x20) = tmp & ~(1 << 2);

	tmp = *(volatile unsigned long *)(0xbfe00100 + 0x1c);
	*(volatile unsigned long *)(0xbfe00100 + 0x1c) = tmp & ~(1 << 2);

#if 0
	longreach = (void *)0xbfc00000;
	(*longreach)();
#endif

	while(1);

}
