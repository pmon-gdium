/*	$Id: tgt_machdep.c,v 1.1.1.1 2006/09/14 01:59:08 root Exp $ */

/*
 * Copyright (c) 2001 Opsycon AB  (www.opsycon.se)
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by Opsycon AB, Sweden.
 * 4. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
#include <include/stdarg.h>
int
tgt_printf (const char *fmt, ...)
{
    int  n;
    char buf[1024];
	char *p=buf;
	char c;
	va_list     ap;
	va_start(ap, fmt);
    n = vsprintf (buf, fmt, ap);
    va_end(ap);
	while((c=*p++))
	{ 
	 if(c=='\n')tgt_putchar('\r');
	 tgt_putchar(c);
	}
    return (n);
}

#if 1
#include <sys/param.h>
#include <sys/syslog.h>
#include <machine/endian.h>
#include <sys/device.h>
#include <machine/cpu.h>
#include <machine/pio.h>
#include <machine/intr.h>
#include <dev/pci/pcivar.h>
#endif
#include <sys/types.h>
#include <termio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include <dev/ic/mc146818reg.h>
#include <linux/io.h>

#include <autoconf.h>

#include <machine/cpu.h>
#include <machine/pio.h>
#include "pflash.h"
#include "dev/pflash_tgt.h"

#include "include/bonito.h"
#include <pmon/dev/gt64240reg.h>
#include <pmon/dev/ns16550.h>

#include <pmon.h>

#include "mod_x86emu_int10.h"
#include "mod_x86emu.h"
#include "mod_vgacon.h"
#include "mod_framebuffer.h"
#include "mod_smi712.h"
#include "mod_smi502.h"
#if PCI_IDSEL_CS5536 != 0
#include <include/cs5536.h>
#endif
#if (NMOD_X86EMU_INT10 > 0)||(NMOD_X86EMU >0)
extern int vga_bios_init(void);
#endif
extern int radeon_init(void);
extern int kbd_initialize(void);
extern int write_at_cursor(char val);
extern const char *kbd_error_msgs[];
#include "flash.h"
#if (NMOD_FLASH_AMD + NMOD_FLASH_INTEL + NMOD_FLASH_SST) == 0
#ifdef HAVE_FLASH
#undef HAVE_FLASH
#endif
#else
#define HAVE_FLASH
#endif

#if (NMOD_X86EMU_INT10 == 0)&&(NMOD_X86EMU == 0)
int vga_available=0;
#else
//#include "vgarom.c"
#endif

volatile char * mmio;

extern unsigned char 
i2c_send_s(unsigned char slave_addr,unsigned char
				sub_addr,unsigned char * buf ,int count);
extern unsigned char 
i2c_send_s16(unsigned char slave_addr,unsigned char
				sub_addr,unsigned short * buf, int count);

#define BCD2BIN(x) (((x&0xf0)>>4)*10+(x&0x0f))
#define BIN2BCD(x)  ((x/10)<<4)+(x%10)

void fixup_sm502_rtc(void)
{
    char  data;
	int tmp;
	pcitag_t tag;

	tag=_pci_make_tag(0,14,0);
	
	_pci_conf_writen(tag, 0x14, 0x6000000, 4);
	tmp = _pci_conf_readn(tag, 0x04, 4);
	tgt_printf("cmd %x\n", tmp);
	_pci_conf_writen(tag, 0x04, tmp |0x02, 4);

	mmio = _pci_conf_readn(tag,0x14,4);

	tgt_printf("mmio %x\n", mmio);

	mmio =(int)mmio|(0xb0000000); /*This is a global variable*/

	//enable gpio clock
	tmp = *(volatile int *)(mmio + 0x40);
	*(volatile int *)(mmio + 0x40) =tmp|0x40;


	data = 0x00;
	i2c_send_s((unsigned char)0xd0,0x0c,&data,1);

	i2c_rec_s((unsigned char)0xd0, 0x01, &data,1);
	data |= 0x80;
	i2c_send_s((unsigned char)0xd0,0x01,&data,1);
	data &= 0x7f;
	i2c_send_s((unsigned char)0xd0,0x01,&data,1);
}

void turn_fan(int on)
{
	unsigned char data8; 
	unsigned short data16;

	i2c_rec_s(0x90, 0x01, &data8, 1);

	/* 
	 * Reg 2,3 is the temparature limit, default is 0x4b00, 0x5000
	 * Reg 0 is current temp
	 * reg 1 is configuration --- valid level
	 */
	if(on) {
		//data16 = 0x320 << 4;
		//data16 = 0x260 << 4; /* 38 */
		data16 = 0x300 << 4; /* 48 */
		i2c_send_s16(0x90, 0x02, &data16, 1); /* Set Thyst*/
		i2c_send_s16(0x90, 0x03, &data16, 1); /* Set Tos */
		data8 &= ~0x04;
		i2c_rec_s(0x90, 0x01, &data8, 1);
	} else {
		data16 = 0x4b00;
		i2c_send_s16(0x90, 0x02, &data16, 1); /* Set Thyst*/

		data16 = 0x5000;
		i2c_send_s16(0x90, 0x03, &data16, 1); /* Set Tos */

		data8 &= ~0x04;
		i2c_send_s((unsigned char)0x90,0x01,&data8, 1);
	}
}

extern struct trapframe DBGREG;
extern void *memset(void *, int, size_t);

int kbd_available;
int usb_kbd_available;;
int vga_available;

static int md_pipefreq = 0;
static int md_cpufreq = 0;
static int clk_invalid = 0;
static int nvram_invalid = 0;
static int cksum(void *p, size_t s, int set);
static void _probe_frequencies(void);

#ifndef NVRAM_IN_FLASH
void nvram_get(char *);
void nvram_put(char *);
#endif

extern int vgaterm(int op, struct DevEntry * dev, unsigned long param, int data);
extern int fbterm(int op, struct DevEntry * dev, unsigned long param, int data);
void error(unsigned long *adr, unsigned long good, unsigned long bad);
void modtst(int offset, int iter, unsigned long p1, unsigned long p2);
void do_tick(void);
void print_hdr(void);
void ad_err2(unsigned long *adr, unsigned long bad);
void ad_err1(unsigned long *adr1, unsigned long *adr2, unsigned long good, unsigned long bad);
void mv_error(unsigned long *adr, unsigned long good, unsigned long bad);

void print_err( unsigned long *adr, unsigned long good, unsigned long bad, unsigned long xor);
static inline unsigned char CMOS_READ(unsigned char addr);
static inline void CMOS_WRITE(unsigned char val, unsigned char addr);
static void init_legacy_rtc(void);

ConfigEntry	ConfigTable[] =
{
#ifndef __NO_UART
#ifndef HAVE_NB_SERIAL
	 { (char *)COM1_BASE_ADDR, 0, ns16550, 256, CONS_BAUD, NS16550HZ/2 },
#else
	 { (char *)COM3_BASE_ADDR, 0, ns16550, 256, CONS_BAUD, NS16550HZ }, 
#endif
#endif
#if NMOD_VGACON >0
#if NMOD_FRAMEBUFFER >0
	{ (char *)1, 0, fbterm, 256, CONS_BAUD, NS16550HZ },
#else
	{ (char *)1, 0, vgaterm, 256, CONS_BAUD, NS16550HZ },
#endif
#endif
	{ 0 }
};

unsigned long _filebase;

extern unsigned int memorysize;
extern unsigned int memorysize_high;

extern char MipsException[], MipsExceptionEnd[];

unsigned char hwethadr[6];

void initmips(unsigned int memsz);

void addr_tst1(void);
void addr_tst2(void);
void movinv1(int iter, ulong p1, ulong p2);

pcireg_t _pci_allocate_io(struct pci_device *dev, vm_size_t size);

extern int log_level;
void
initmips(unsigned int memsz)
{

#if 0 //def DEVBD2F_SM502
{
/*set lio bus to 16 bit*/
volatile int *p=0xbfe00108;
*p=((*p)&~(0x1f<<8))|(0x8<<8) |(1<<13);
}
#endif

	log_level = 2;
/*enable float*/
tgt_fpuenable();
	/*
	 *	Set up memory address decoders to map entire memory.
	 *	But first move away bootrom map to high memory.
	 */
#if 0
	GT_WRITE(BOOTCS_LOW_DECODE_ADDRESS, BOOT_BASE >> 20);
	GT_WRITE(BOOTCS_HIGH_DECODE_ADDRESS, (BOOT_BASE - 1 + BOOT_SIZE) >> 20);
#endif
	memorysize = memsz > 256 ? 256 << 20 : memsz << 20;
	memorysize_high = memsz > 256 ? (memsz - 256) << 20 : 0;

#if defined(DEVBD2F_SM502)
	fixup_sm502_rtc();
	turn_fan(0);
#endif

#if 0
asm(\
"	 sd %1,0x18(%0);;\n" \
"	 sd %2,0x28(%0);;\n" \
"	 sd %3,0x20(%0);;\n" \
	 ::"r"(0x900000003ff00000ULL),"r"(memorysize),"r"(memorysize_high),"r"(0x20000000)
	 :"$2"
   );
#endif

#if 0
	{
	  int start = 0x80000000;
	  int end = 0x80000000 + 16384;

	  while (start < end) {
	    __asm__ volatile (" cache 1,0(%0)\r\n"
		              " cache 1,1(%0)\r\n"
		              " cache 1,2(%0)\r\n"
		              " cache 1,3(%0)\r\n"
		              " cache 0,0(%0)\r\n"::"r"(start));
	    start += 32;
	  }

	  __asm__ volatile ( " mfc0 $2,$16\r\n"
	                   " and  $2, $2, 0xfffffff8\r\n"
			   " or   $2, $2, 2\r\n"
			   " mtc0 $2, $16\r\n" :::"$2");
	}
#endif

	/*
	 *  Probe clock frequencys so delays will work properly.
	 */
	tgt_cpufreq();
	SBD_DISPLAY("DONE",0);


	/*
	 *  Init PMON and debug
	 */
	cpuinfotab[0] = &DBGREG;
	dbginit(NULL);

	/*
	 *  Set up exception vectors.
	 */
	SBD_DISPLAY("BEV1",0);
	bcopy(MipsException, (char *)TLB_MISS_EXC_VEC, MipsExceptionEnd - MipsException);
	bcopy(MipsException, (char *)GEN_EXC_VEC, MipsExceptionEnd - MipsException);

	CPU_FlushCache();

	CPU_SetSR(0, SR_BOOT_EXC_VEC);
	SBD_DISPLAY("BEV0",0);
	
	printf("BEV in SR set to zero.\n");

#if 0
	{
		extern int startiic(int argc,char **argv);
		unsigned char value;
		char *argv[] = {"startiic", "80"};

		if(!getenv("updateec")) {
			flash_recv(0x80, 0, &value);
			if(value != 0x11)
				startiic(2, argv);
		}
	}
#endif
		

	
#if 0
	/* memtest */
	addr_tst1();
	addr_tst2();
	movinv1(2,0,~0);
	movinv1(2,0xaa5555aa,~0xaa5555aa);
	printf("memtest done\n");
#endif

if(getenv("powermg"))
{
pcitag_t mytag;
unsigned char data;
unsigned int addr;
	mytag=_pci_make_tag(0,17,4);
	data=_pci_conf_readn(mytag,0x41,1);
	_pci_conf_writen(mytag,0x41,data|0x80,1);
	addr=_pci_allocate_io(_pci_head,256);
	printf("power management addr=%x\n",addr);
	_pci_conf_writen(mytag,0x48,addr|1,4);
}

	/*
	 * Launch!
	 */
	main();
}

/*
 *  Put all machine dependent initialization here. This call
 *  is done after console has been initialized so it's safe
 *  to output configuration and debug information with printf.
 */
extern void	vt82c686_init(void);
int psaux_init(void);
extern int video_hw_init (void);

extern int fb_init(unsigned long,unsigned long);
void
tgt_devconfig()
{
#if NMOD_VGACON > 0
	int rc=1;
#if NMOD_FRAMEBUFFER > 0 
	unsigned long fbaddress,ioaddress;
	extern struct pci_device *vga_dev;
#endif
#endif
	_pci_devinit(1);	/* PCI device initialization */
#if (NMOD_X86EMU_INT10 > 0)||(NMOD_X86EMU >0)
	SBD_DISPLAY("VGAI", 0);
	rc = vga_bios_init();
#endif
#if (NMOD_X86EMU_INT10 == 0 && defined(RADEON7000))
	SBD_DISPLAY("VGAI", 0);
	rc = radeon_init();
#endif
#if NMOD_FRAMEBUFFER > 0
	if(!vga_dev) {
		printf("ERROR !!! VGA device is not found\n"); 
		rc = -1;
	}
	if (rc > 0) {
		fbaddress  =_pci_conf_read(vga_dev->pa.pa_tag,0x10);
		ioaddress  =_pci_conf_read(vga_dev->pa.pa_tag,0x18);

		fbaddress = fbaddress &0xffffff00; //laster 8 bit
		ioaddress = ioaddress &0xfffffff0; //laster 4 bit

#if NMOD_SMI712 > 0
		fbaddress |= 0xb0000000;
		ioaddress |= 0xbfd00000;
        smi712_init((unsigned char *)fbaddress,(unsigned char *)ioaddress);

#endif

#if NMOD_SMI502 > 0
        rc = video_hw_init ();
                fbaddress  =_pci_conf_read(vga_dev->pa.pa_tag,0x10);
                ioaddress  =_pci_conf_read(vga_dev->pa.pa_tag,0x14);
                fbaddress |= 0xb0000000;
                ioaddress |= 0xb0000000;
#if 1
		/*lit LCD and turn on audio*/
		{
			unsigned long tag;
			unsigned int mmio, tmp;

			tag=_pci_make_tag(0,14,0);
			mmio = _pci_conf_readn(tag,0x14,4);
			mmio =(int)mmio|(0xb0000000);
			tmp = *(volatile int *)(mmio + 0x10008);
			*(volatile int *)(mmio + 0x10008) = tmp|((1<<29)|(1<<31));
			tmp = *(volatile int *)(mmio + 0x10000);
			*(volatile int *)(mmio + 0x10000) = tmp|((1<<29)|(1<<31));
		}
#endif

#endif
		fb_init(fbaddress, ioaddress);
		vga_available = 0;
	} else {
		printf("vga bios init failed, rc=%d\n",rc);
	}
#endif

#if (NMOD_FRAMEBUFFER > 0) || (NMOD_VGACON > 0 )
    if (rc > 0) {
		if(!getenv("novga")) 
			vga_available=1;
		else 
			vga_available=0;
	} 
#endif
    config_init();
    configure();

	log_level = 2;
#if ((NMOD_VGACON >0) &&(PCI_IDSEL_VIA686B !=0)|| (PCI_IDSEL_CS5536 !=0))
	if(getenv("nokbd")) rc=1;
	else rc=kbd_initialize();
	printf("%s\n",kbd_error_msgs[rc]);
	if(!rc){ 
		kbd_available=1;
	}
//	psaux_init();
#endif
}

extern int test_icache_1(short *addr);
extern int test_icache_2(int addr);
extern int test_icache_3(int addr);
extern void godson1_cache_flush(void);
#define tgt_putchar_uc(x) (*(void (*)(char)) (((long)tgt_putchar)|0x20000000)) (x)

extern void cs5536_gpio_init(void);
extern void test_gpio_function(void);
extern void cs5536_pci_fixup(void);

#if PCI_IDSEL_CS5536 != 0
static int w83627_read(int dev,int addr)
{
int data;
/*enter*/
outb(0xbfd0002e,0x87);
outb(0xbfd0002e,0x87);
/*select logic dev reg */
outb(0xbfd0002e,0x7);
outb(0xbfd0002f,dev);
/*access reg */
outb(0xbfd0002e,addr);
data=inb(0xbfd0002f);
/*exit*/
outb(0xbfd0002e,0xaa);
outb(0xbfd0002e,0xaa);
return data;
}

static void w83627_write(int dev,int addr,int data)
{
/*enter*/
outb(0xbfd0002e,0x87);
outb(0xbfd0002e,0x87);
/*select logic dev reg */
outb(0xbfd0002e,0x7);
outb(0xbfd0002f,dev);
/*access reg */
outb(0xbfd0002e,addr);
outb(0xbfd0002f,data);
/*exit*/
outb(0xbfd0002e,0xaa);
outb(0xbfd0002e,0xaa);
}
#endif
void
tgt_devinit()
{
#if  (PCI_IDSEL_VIA686B != 0)
	SBD_DISPLAY("686I",0);
	
	vt82c686_init();
#endif

#if  (PCI_IDSEL_CS5536 != 0)
	SBD_DISPLAY("5536",0);
	cs5536_init();
#endif

#if PCI_IDSEL_CS5536 != 0
w83627_write(0,0x24,0xc1);
w83627_write(5,0x30,1);
w83627_write(5,0x60,0);
w83627_write(5,0x61,0x60);
w83627_write(5,0x62,0);
w83627_write(5,0x63,0x64);
w83627_write(5,0x70,1);
w83627_write(5,0x72,0xc);
w83627_write(5,0xf0,0x80);
_wrmsr(GET_MSR_ADDR(0x5140001F), 0, 0);//no keyboard emulation

#ifndef USE_CS5536_UART
w83627_write(2,0x30,0x01);
w83627_write(2,0x60,0x03);
w83627_write(2,0x61,0xf8);
w83627_write(2,0x70,0x04);
w83627_write(2,0xf0,0x00);
#endif

#endif

	/*
	 *  Gather info about and configure caches.
	 */
	if(getenv("ocache_off")) {
		CpuOnboardCacheOn = 0;
	}
	else {
		CpuOnboardCacheOn = 1;
	}
	if(getenv("ecache_off")) {
		CpuExternalCacheOn = 0;
	}
	else {
		CpuExternalCacheOn = 1;
	}
	
    CPU_ConfigCache();

	_pci_businit(1);	/* PCI bus initialization */
#if defined(VIA686B_POWERFIXUP) && (PCI_IDSEL_VIA686B != 0)
if(getenv("noautopower"))	vt82c686_powerfixup();
#endif

#if  (PCI_IDSEL_CS5536 != 0)
	cs5536_pci_fixup();
#endif
}


#ifdef DEVBD2F_CS5536
void
tgt_reboot()
{
	unsigned long hi, lo;
	
	/* reset the cs5536 whole chip */
	_rdmsr(0xe0000014, &hi, &lo);
	lo |= 0x00000001;
	_wrmsr(0xe0000014, hi, lo);

	while(1);
}

void
tgt_poweroff()
{
	unsigned long val;
	unsigned long tag;
	unsigned long base;

	tag = _pci_make_tag(0, 14, 0);
	base = _pci_conf_read(tag, 0x14);
	base |= 0xbfd00000;
	base &= ~3;

	/* make cs5536 gpio13 output enable */
	val = *(volatile unsigned long *)(base + 0x04);
	val = ( val & ~(1 << (16 + 13)) ) | (1 << 13) ;
	*(volatile unsigned long *)(base + 0x04) = val;
	
	/* make cs5536 gpio13 output low level voltage. */
	val = *(volatile unsigned long *)(base + 0x00);
	val = (val | (1 << (16 + 13))) & ~(1 << 13);
	*(volatile unsigned long *)(base + 0x00) = val;

	while(1);
}
#elif defined(DEVBD2F_SM502)
void
tgt_poweroff()
{
#if	0
	volatile int *p=0xbfe0011c;
	p[1]&=~1;
	p[0]&=~1;
	p[0]|=1;
#else
	unsigned long val;

	val = *((volatile unsigned long *)(0xbfe0011c));
	*((volatile unsigned long *)(0xbfe0011c)) = val | (1 << 1);
	val = *((volatile unsigned long *)(0xbfe00120));
	*((volatile unsigned long *)(0xbfe00120)) = val & (~(1 << 1));
#endif
}
#endif


/*
 *  This function makes inital HW setup for debugger and
 *  returns the apropriate setting for the status register.
 */
register_t
tgt_enable(int machtype)
{
	/* XXX Do any HW specific setup */
	return(SR_COP_1_BIT|SR_FR_32|SR_EXL);
}


/*
 *  Target dependent version printout.
 *  Printout available target version information.
 */
void
tgt_cmd_vers()
{
}

/*
 *  Display any target specific logo.
 */
void
tgt_logo()
{
#if 0
    printf("\n");
    printf("[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[\n");
    printf("[[[            [[[[   [[[[[[[[[[   [[[[            [[[[   [[[[[[[  [[\n");
    printf("[[   [[[[[[[[   [[[    [[[[[[[[    [[[   [[[[[[[[   [[[    [[[[[[  [[\n");
    printf("[[  [[[[[[[[[[  [[[  [  [[[[[[  [  [[[  [[[[[[[[[[  [[[  [  [[[[[  [[\n");
    printf("[[  [[[[[[[[[[  [[[  [[  [[[[  [[  [[[  [[[[[[[[[[  [[[  [[  [[[[  [[\n");
    printf("[[   [[[[[[[[   [[[  [[[  [[  [[[  [[[  [[[[[[[[[[  [[[  [[[  [[[  [[\n");
    printf("[[             [[[[  [[[[    [[[[  [[[  [[[[[[[[[[  [[[  [[[[  [[  [[\n");
    printf("[[  [[[[[[[[[[[[[[[  [[[[[  [[[[[  [[[  [[[[[[[[[[  [[[  [[[[[  [  [[\n");
    printf("[[  [[[[[[[[[[[[[[[  [[[[[[[[[[[[  [[[  [[[[[[[[[[  [[[  [[[[[[    [[\n");
    printf("[[  [[[[[[[[[[[[[[[  [[[[[[[[[[[[  [[[   [[[[[[[[   [[[  [[[[[[[   [[\n");
    printf("[[  [[[[[[[[[[[[[[[  [[[[[[[[[[[[  [[[[            [[[[  [[[[[[[[  [[\n");
    printf("[[[[[[[2005][[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[\n"); 
#endif
}

static void init_legacy_rtc(void)
{
        int year, month, date, hour, min, sec;
        CMOS_WRITE(DS_CTLA_DV1, DS_REG_CTLA);
        CMOS_WRITE(DS_CTLB_24 | DS_CTLB_DM | DS_CTLB_SET, DS_REG_CTLB);
        CMOS_WRITE(0, DS_REG_CTLC);
        CMOS_WRITE(0, DS_REG_CTLD);
        year = CMOS_READ(DS_REG_YEAR);
        month = CMOS_READ(DS_REG_MONTH);
        date = CMOS_READ(DS_REG_DATE);
        hour = CMOS_READ(DS_REG_HOUR);
        min = CMOS_READ(DS_REG_MIN);
        sec = CMOS_READ(DS_REG_SEC);
        if( (year > 99) || (month < 1 || month > 12) ||
                (date < 1 || date > 31) || (hour > 23) || (min > 59) ||
                (sec > 59) ){
                /*
                printf("RTC time invalid, reset to epoch.\n");*/
                CMOS_WRITE(3, DS_REG_YEAR);
                CMOS_WRITE(1, DS_REG_MONTH);
                CMOS_WRITE(1, DS_REG_DATE);
                CMOS_WRITE(0, DS_REG_HOUR);
                CMOS_WRITE(0, DS_REG_MIN);
                CMOS_WRITE(0, DS_REG_SEC);
        }
        CMOS_WRITE(DS_CTLB_24 | DS_CTLB_DM, DS_REG_CTLB);
                                                                               
	//printf("RTC: %02d-%02d-%02d %02d:%02d:%02d\n",
	//    year, month, date, hour, min, sec);
}

static inline unsigned char CMOS_sec()
{
	int val;

	i2c_rec_s((unsigned char)0xd0,0,&val,1);

	return BCD2BIN(val);
}

static inline unsigned char CMOS_READ(unsigned char addr)
{
	unsigned char val;
	volatile int tmp;
//	unsigned char and_char;
	
#ifndef DEVBD2F_SM502
	linux_outb_p(addr, 0x70);
        val = linux_inb_p(0x71);
#else

	pcitag_t tag;
	if(addr >= 32)
		return 0;
	switch(addr)
	{
		case 0:
			addr = 0x01;
			break;
		case 1:
			addr = 0x0e;
			break;
		case 2:
			break;
		case 3:
			addr = 0x0d;
			break;
		case 4:
			addr = 0x03;
			break;
		case 5:
			addr = 0x0c;
			break;
		case 6:
			addr = 0x04;
			break;
		case 7:
			addr = 0x05;
			break;
		case 8:
			addr = 0x06;
			break;
		case 9:
		    addr = 0x07;
			break;
		case 13:
			addr = 0x0f;
			break;	
	}

#if 1
	tag=_pci_make_tag(0,14,0);
	
	mmio = _pci_conf_readn(tag,0x14,4);
	mmio =(int)mmio|(0xb0000000);
	tmp = *(volatile int *)(mmio + 0x40);
	*(volatile int *)(mmio + 0x40) =tmp|0x40;
#endif
		
	i2c_rec_s((unsigned char)0xd0,addr,&val,1);
	if(addr == 0x0f) {
		if(!(val&0x10))
			val = 0x80;
		else
			val = 0x00;
	}
	if(addr == 0x0d || addr == 0x0e)
		val &= ~0x80;
	if(addr == 0x0b)
		val &= 0x3f;
	
#endif
	return val;
}
                                                                               
static inline void CMOS_WRITE(unsigned char val, unsigned char addr)
{
#ifndef DEVBD2F_SM502
	linux_outb_p(addr, 0x70);
        linux_outb_p(val, 0x71);
#else

	unsigned char tmp1,tmp2;
	volatile int tmp;
	tmp1 = (val/10)<<4;
	tmp2  = (val%10);
	val = tmp1|tmp2;
	if(addr >=32)
		return ;
	switch(addr)
	{
		case 0:
			addr = 0x01;
			break;
		case 1:
			addr = 0x0e;
			break;
		case 2:
			break;
		case 3:
			addr = 0x0d;
			break;
		case 4:
			addr = 0x03;
			break;
		case 5:
			addr = 0x0c;
			break;
		case 6:
			addr = 0x04;		
			break;
		case 7:
			addr = 0x05;
			break;
		case 8:
			addr = 0x06;
			break;
		case 9:
			addr = 0x07;
			break;
		case 13:
			addr = 0x0f;
	}

	{
		pcitag_t tag;
		unsigned char value;
		tag=_pci_make_tag(0,14,0);
	
		mmio = _pci_conf_readn(tag,0x14,4);
		mmio =(int)mmio|(0xb0000000);
		tmp = *(volatile int *)(mmio + 0x40);
		*(volatile int *)(mmio + 0x40) =tmp|0x40;

		i2c_send_s((unsigned char)0xd0,addr,&val,1);
	}
#endif
}

static void
_probe_frequencies()
{
#ifdef HAVE_TOD
        int i, timeout, cur, sec, cnt;
		int __sec, __sec2; /*0.01 sec*/
#endif
                                                                    
        SBD_DISPLAY ("FREQ", CHKPNT_FREQ);

#if 0
        md_pipefreq = 300000000;        /* Defaults */
        md_cpufreq  = 66000000;
#else
        md_pipefreq = 660000000;        /* NB FPGA*/
        md_cpufreq  =  60000000;
#endif

        clk_invalid = 1;
#ifdef HAVE_TOD
        //init_legacy_rtc();

        SBD_DISPLAY ("FREI", CHKPNT_FREQ);

        /*
         * Do the next twice for two reasons. First make sure we run from
         * cache. Second make sure synched on second update. (Pun intended!)
         */
        for(i = 2;  i != 0; i--) {
                cnt = CPU_GetCOUNT();
                timeout = 10000000;
#ifndef DEVBD2F_SM502
                while(CMOS_READ(DS_REG_CTLA) & DS_CTLA_UIP);
#endif
                                                                               
                sec = CMOS_READ(DS_REG_SEC);
#ifdef DEVBD2F_SM502
				sec = ((sec & 0xf0) >> 4) * 10 + (sec & 0x0f);
				__sec = CMOS_sec();
#endif
                do {
                        timeout--;
			
#ifndef DEVBD2F_SM502
                        while(CMOS_READ(DS_REG_CTLA) & DS_CTLA_UIP);
#endif
                        cur = CMOS_READ(DS_REG_SEC);
						__sec2  = CMOS_sec();
#ifdef DEVBD2F_SM502
						cur = ((cur & 0xf0) >> 4)* 10 + (cur & 0x0f);
						if(cur < sec) cur += 60;
#endif
                //} while(timeout != 0 && ((cur == sec)||(cur !=(sec+1))));
                } while(timeout != 0 && ((cur - sec)*100+(__sec2-__sec)) < 50);

				cnt = CPU_GetCOUNT() - cnt;
                if(timeout == 0) {
						tgt_printf("time out!\n");
                        break;          /* Get out if clock is not running */
                }
        }
                                                                               
	/*
	 *  Calculate the external bus clock frequency.
	 */
	if (timeout != 0) {
		unsigned int tsec = (cur-sec)*100+(__sec2-__sec);

		clk_invalid = 0;
		md_pipefreq = cnt / 10000;
		md_pipefreq *= 20000;

		md_pipefreq /= tsec;
		md_pipefreq *= 100;
		/* we have no simple way to read multiplier value
		 */
		md_cpufreq = 66000000;
	}
#endif /* HAVE_TOD */
}
                                                                               

/*
 *   Returns the CPU pipelie clock frequency
 */
int
tgt_pipefreq()
{
	if(md_pipefreq == 0) {
		_probe_frequencies();
	}
	return(md_pipefreq);
}

/*
 *   Returns the external clock frequency, usually the bus clock
 */
int
tgt_cpufreq()
{
	if(md_cpufreq == 0) {
		_probe_frequencies();
	}
	return(md_cpufreq);
}

time_t
tgt_gettime()
{
        struct tm tm;
        int ctrlbsave;
        time_t t;

	/*gx 2005-01-17 */
	//return 0;
                                                                               
#ifdef HAVE_TOD
        if(!clk_invalid) {
#ifndef DEVBD2F_SM502
                ctrlbsave = CMOS_READ(DS_REG_CTLB);
                CMOS_WRITE(ctrlbsave | DS_CTLB_SET, DS_REG_CTLB);
#endif
                                                                               
                tm.tm_sec = CMOS_READ(DS_REG_SEC);
                tm.tm_min = CMOS_READ(DS_REG_MIN);
                tm.tm_hour = CMOS_READ(DS_REG_HOUR);
                tm.tm_wday = CMOS_READ(DS_REG_WDAY);
                tm.tm_mday = CMOS_READ(DS_REG_DATE);
                tm.tm_mon = CMOS_READ(DS_REG_MONTH) - 1;
                tm.tm_year = CMOS_READ(DS_REG_YEAR);
#ifdef DEVBD2F_SM502
				tm.tm_sec =  BCD2BIN(tm.tm_sec);
				tm.tm_min =  BCD2BIN(tm.tm_min);
				tm.tm_hour = BCD2BIN(tm.tm_hour);
				//tm.tm_wday = BCD2BIN(tm.tm_wday);
				tm.tm_mon = BCD2BIN(tm.tm_mon);
				tm.tm_mday = BCD2BIN(tm.tm_mday);
				tm.tm_year = BCD2BIN(tm.tm_year);
#endif
                if(tm.tm_year < 50)tm.tm_year += 100;
                                                                               
#if 0
                CMOS_WRITE(ctrlbsave & ~DS_CTLB_SET, DS_REG_CTLB);
#endif
                                                                               
                tm.tm_isdst = tm.tm_gmtoff = 0;
                t = gmmktime(&tm);
        }
        else 
#endif
		{
                t = 957960000;  /* Wed May 10 14:00:00 2000 :-) */
        }
        return(t);
}
                                                                               
/*
 *  Set the current time if a TOD clock is present
 */
void
tgt_settime(time_t t)
{
        struct tm *tm;
        int ctrlbsave;

	//return ;
                                                                               
#ifdef HAVE_TOD
        if(!clk_invalid) {
                tm = gmtime(&t);
                ctrlbsave = CMOS_READ(DS_REG_CTLB);
                CMOS_WRITE(ctrlbsave | DS_CTLB_SET, DS_REG_CTLB);
                                                                               
                CMOS_WRITE(tm->tm_year % 100, DS_REG_YEAR);
                CMOS_WRITE(tm->tm_mon + 1, DS_REG_MONTH);
                CMOS_WRITE(tm->tm_mday, DS_REG_DATE);
                CMOS_WRITE(tm->tm_wday, DS_REG_WDAY);
                CMOS_WRITE(tm->tm_hour, DS_REG_HOUR);
                CMOS_WRITE(tm->tm_min, DS_REG_MIN);
                CMOS_WRITE(tm->tm_sec, DS_REG_SEC);
                                                                               
                CMOS_WRITE(ctrlbsave & ~DS_CTLB_SET, DS_REG_CTLB);
        }
#endif
}


/*
 *  Print out any target specific memory information
 */
void
tgt_memprint()
{
	printf("Primary Instruction cache size %dkb (%d line, %d way)\n",
		CpuPrimaryInstCacheSize / 1024, CpuPrimaryInstCacheLSize, CpuNWayCache);
	printf("Primary Data cache size %dkb (%d line, %d way)\n",
		CpuPrimaryDataCacheSize / 1024, CpuPrimaryDataCacheLSize, CpuNWayCache);
	if(CpuSecondaryCacheSize != 0) {
		printf("Secondary cache size %dkb\n", CpuSecondaryCacheSize / 1024);
	}
	if(CpuTertiaryCacheSize != 0) {
		printf("Tertiary cache size %dkb\n", CpuTertiaryCacheSize / 1024);
	}
}

void
tgt_machprint()
{
	printf("Copyright 2000-2002, Opsycon AB, Sweden.\n");
	printf("Copyright 2005, ICT CAS.\n");
	printf("CPU %s @", md_cpuname());
} 

/*
 *  Return a suitable address for the client stack.
 *  Usually top of RAM memory.
 */

register_t
tgt_clienttos()
{
	return((register_t)(int)PHYS_TO_UNCACHED(memorysize & ~7) - 64);
}

#ifdef HAVE_FLASH
/*
 *  Flash programming support code.
 */

/*
 *  Table of flash devices on target. See pflash_tgt.h.
 */

struct fl_map tgt_fl_map_boot16[]={
	TARGET_FLASH_DEVICES_16
};


struct fl_map *
tgt_flashmap()
{
	return tgt_fl_map_boot16;
}   
void
tgt_flashwrite_disable()
{
}

int
tgt_flashwrite_enable()
{
	return(1);
}

void
tgt_flashinfo(void *p, size_t *t)
{
	struct fl_map *map;

	map = fl_find_map(p);
	if(map) {
		*t = map->fl_map_size;
	}
	else {
		*t = 0;
	}
}

void
tgt_flashprogram(void *p, int size, void *s, int endian)
{
	printf("Programming flash %x:%x into %x\n", s, size, p);
	if(fl_erase_device(p, size, TRUE)) {
		printf("Erase failed!\n");
		return;
	}
	if(fl_program_device(p, s, size, TRUE)) {
		printf("Programming failed!\n");
	}
	fl_verify_device(p, s, size, TRUE);
}
#endif /* PFLASH */

#if	1 /* flash st7 support */
/* please check the cpu frequency */
#define	ST7_PID_0	0x47	// 'G'
#define	ST7_PID_1	0x44	// 'D'
#define	ST7_PID_2	0x49	// 'I'
#define	ST7_PID_3	0x55	// 'U'
#define	ST7_PID_4	0x4D	// 'M'
#define	ST7_PID_S	0x53	// 'S'
#define	ST7_PID_E	0x45	// 'E'
#define	ST7_HID_N	0x4E	// 'N'
#define	ST7_HID_O	0x4F	// 'O'
#define	ST7_HID_R	0x52	// 'R'
#define	ST7_HID_M	0x4D	// 'M'
#define	ST7_HID_A	0x41	// 'A'
#define	ST7_HID_L	0x4C	// 'L'
#define	ST7_ACK_START	0xaa	// start ack
#define	ST7_ACK_DATA	0xdd	// data ack
#define	ST7_ACK_END	0xee	// end ack
#define	ST7_ACK_ERROR	0xff	// Error ack with size
extern int flash_send(unsigned char slave_addr, unsigned char index_addr, unsigned char value);
extern int flash_recv(unsigned char slave_addr, unsigned char index_addr, unsigned char *value);
#define	ST7_MAX_SIZE		0x1000	// 4KB for max whole chip size
#define	ST7_PROGRAM_SIZE	0xc00	// 3KB for program
#define	ST7_BLOCK_SIZE		0x20	// 32bytes for one block
#define	ST7_SLAVE_ADDRESS	0x80
#define	ST7_ACK_REG		0x21
int tgt_st7program(void *s, int size)
{
	volatile unsigned char *ptr = s;
	volatile unsigned char buffer[ST7_PROGRAM_SIZE];
	pcitag_t tag;
	unsigned long temp;
	int ret = 0;
	int i;
	unsigned char check_sum = 0;
	unsigned char value;
	unsigned long block_num = 0;
	unsigned long all_blocks = 0;
	unsigned long program_size = 0;
	int check_timeout = 0;
	
	/* set the mmio base and config the gpio clock */
	tag  = _pci_make_tag(0, 14, 0);
	mmio = _pci_conf_readn(tag, 0x14, 4);
	mmio = (int)mmio | 0xb0000000;
	temp = _pci_conf_readn(tag, 0x04, 4);
	_pci_conf_writen(tag, 0x04, temp | 0x07, 0x04);
	temp = _pci_conf_readn(tag, 0x04, 4);
	
	/* check the size of program */
	if(size > ST7_MAX_SIZE){
		printf("st7 : program size is too large.\n");
		return 1;
	}
	if(size < 0x100){
		printf("st7 : program size is too small.\n");
		return 1;
	}

	for(i = 0; i < ST7_PROGRAM_SIZE; i++)
		buffer[i] = 0xff;
	for(i = 0; i < size; i++)
		buffer[i] = ptr[i];
	for(i = 0; i < ST7_PROGRAM_SIZE; i++){
		check_sum += buffer[i];
	}	
	ptr = &buffer[0];
	program_size = ST7_PROGRAM_SIZE;
	printf("st7 : program size 0x%x, src addr 0x%x\n", program_size, (unsigned long)ptr);

	/* start program stage */
	ret = flash_send(ST7_SLAVE_ADDRESS, 0, ST7_PID_0);
	if(ret){
		printf("st7 : start program 0 error.\n");
		return 2;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 1, ST7_PID_1);
	if(ret){
		printf("st7 : start program 1 error.\n");
		return 2;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 2, ST7_PID_2);
	if(ret){
		printf("st7 : start program 2 error.\n");
		return 2;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 3, ST7_PID_3);
	if(ret){
		printf("st7 : start program 3 error.\n");
		return 2;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 4, ST7_PID_4);
	if(ret){
		printf("st7 : start program 4 error.\n");
		return 2;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 5, ST7_PID_S);
	if(ret){
		printf("st7 : start program 5 error.\n");
		return 2;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 6, (unsigned char)( (program_size & 0xff00) >> 8) );
	if(ret){
		printf("st7 : start program 6 error.\n");
		return 2;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 7, (unsigned char)(program_size & 0x00ff));
	
	/* check the ack */
	while(value != ST7_ACK_START){
		ret = flash_recv(ST7_SLAVE_ADDRESS, ST7_ACK_REG, &value);
		if(ret){
			printf("st7 : start ack error.\n");
			return 2;
		}
		delay(1000);	// delay 1ms for not busy
		if(value == ST7_ACK_ERROR){
			printf("st7 : size error, should be with 32bytes unit.\n");
			return 2;
		}
	}

	printf("stage 1(start) over.\n");

	/* program flash stage : calculate block number*/
	all_blocks = program_size / ST7_BLOCK_SIZE;
	for( block_num = 0; block_num < all_blocks; block_num++ ){
		/* program each block */
		for(i = 0; i < ST7_BLOCK_SIZE; i++){
			ret = flash_send(ST7_SLAVE_ADDRESS, i, *ptr++);
			if(ret){
				printf("st7 : program block %d error.\n", block_num);
				return 3;
			}
		}
		/* add the pad for st7 bug */
		ret = flash_send(ST7_SLAVE_ADDRESS, ST7_BLOCK_SIZE, 0x55);
		if(ret){
			printf("st7 : program pad error.\n");
			return 3;
		}
		/* check the ack */
		while(value != ST7_ACK_DATA){
			ret = flash_recv(ST7_SLAVE_ADDRESS, ST7_ACK_REG, &value);
			if(ret){
				printf("st7 : program ack error.\n");
				return 3;
			}
			delay(1000);	// delay 1ms for not busy
		}
		printf(".");
	}

	printf("\nstage 2(program) over.\n");

	/* end program stage */
	ret = flash_send(ST7_SLAVE_ADDRESS, 0, ST7_PID_0);
	if(ret){
		printf("st7 : end program 0 error.\n");
		return 4;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 1, ST7_PID_1);
	if(ret){
		printf("st7 : end program 1 error.\n");
		return 4;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 2, ST7_PID_2);
	if(ret){
		printf("st7 : end program 2 error.\n");
		return 4;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 3, ST7_PID_3);
	if(ret){
		printf("st7 : end program 3 error.\n");
		return 4;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 4, ST7_PID_4);
	if(ret){
		printf("st7 : end program 4 error.\n");
		return 4;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 5, ST7_PID_E);
	if(ret){
		printf("st7 : end program 5 error.\n");
		return 4;
	}

	/* check the end ack */
	check_timeout = 100;
	while(check_timeout--){
		ret = flash_recv(ST7_SLAVE_ADDRESS, ST7_ACK_REG, &value);
		if(ret){
			printf("st7 : end ack error.\n");
			return 4;
		}
		delay(1000);	// delay 1ms for not busy
		if(value == check_sum){
			printf("st7 : check sum ok.\n");
			break;
		}
	}
	if(check_timeout <= 0){
		printf("st7 :check sum = 0x%x, recv check sum 0x%x\n", check_sum, value);
		printf("st7 : check sum error, ST7 can't work normally, re-program it after all power off.\n");
	}
	printf("stage 3(checksum) over.\n");

	/* checksum send stage */
	ret = flash_send(ST7_SLAVE_ADDRESS, 0, 'O');
	if(ret){
		printf("st7 : checksum program 0 error.\n");
		return 4;
	}
	ret = flash_send(ST7_SLAVE_ADDRESS, 1, 'K');
	if(ret){
		printf("st7 : checksum program 1 error.\n");
		return 4;
	}
	printf("stage 4(end) over.\n");

	if(!ret)
		printf("st7 : program successful.\n");
		
	return 0;
}
#else
void tgt_st7program(void *s, int size)
{
	printf("st7 : not implemented yet!\n");
	return;
}
#endif
/*
 *  Network stuff.
 */
void
tgt_netinit()
{
}

int
tgt_ethaddr(char *p)
{
	bcopy((void *)&hwethadr, p, 6);
	return(0);
}

void
tgt_netreset()
{
}

/*************************************************************************/
/*
 *	Target dependent Non volatile memory support code
 *	=================================================
 *
 *
 *  On this target a part of the boot flash memory is used to store
 *  environment. See EV64260.h for mapping details. (offset and size).
 */

/*
 *  Read in environment from NV-ram and set.
 */
void
tgt_mapenv(int (*func) __P((char *, char *)))
{
	char *ep;
	char env[512];
	char *nvram;
	int i;

        /*
         *  Check integrity of the NVRAM env area. If not in order
         *  initialize it to empty.
         */
#ifdef NVRAM_IN_FLASH
    nvram = (char *)(tgt_flashmap())->fl_map_base;
	printf("nvram=%08x\n",(unsigned int)nvram);
	if(fl_devident(nvram, NULL) == 0 ||
           cksum(nvram + NVRAM_OFFS, NVRAM_SIZE, 0) != 0) {
#else
    nvram = (char *)malloc(512);
	nvram_get(nvram);
	if(cksum(nvram, NVRAM_SIZE, 0) != 0) {
#endif
		        printf("NVRAM is invalid!\n");
                nvram_invalid = 1;
        }
        else {
				nvram += NVRAM_OFFS;
                ep = nvram+2;;

                while(*ep != 0) {
                        char *val = 0, *p = env;
						i = 0;
                        while((*p++ = *ep++) && (ep <= nvram + NVRAM_SIZE - 1) && i++ < 255) {
                                if((*(p - 1) == '=') && (val == NULL)) {
                                        *(p - 1) = '\0';
                                        val = p;
                                }
                        }
                        if(ep <= nvram + NVRAM_SIZE - 1 && i < 255) {
                                (*func)(env, val);
                        }
                        else {
                                nvram_invalid = 2;
                                break;
                        }
                }
        }

	printf("NVRAM@%x\n",(u_int32_t)nvram);

	/*
	 *  Ethernet address for Galileo ethernet is stored in the last
	 *  six bytes of nvram storage. Set environment to it.
	 */
#if 0
	bcopy(&nvram[ETHER_OFFS], hwethadr, 6);
	sprintf(env, "%02x:%02x:%02x:%02x:%02x:%02x", hwethadr[0], hwethadr[1],
	    hwethadr[2], hwethadr[3], hwethadr[4], hwethadr[5]);
	(*func)("ethaddr", env);
#endif

#ifndef NVRAM_IN_FLASH
	free(nvram);
#endif

#ifdef no_thank_you
    (*func)("vxWorks", env);
#endif


	sprintf(env, "%d", memorysize / (1024 * 1024));
	(*func)("memsize", env);

	sprintf(env, "%d", memorysize_high / (1024 * 1024));
	(*func)("highmemsize", env);

	sprintf(env, "%d", md_pipefreq);
	(*func)("cpuclock", env);

	sprintf(env, "%d", md_cpufreq);
	(*func)("busclock", env);

	(*func)("systype", SYSTYPE);
	
}

int
tgt_unsetenv(char *name)
{
        char *ep, *np, *sp;
        char *nvram;
        char *nvrambuf;
        char *nvramsecbuf;
	int status;

        if(nvram_invalid) {
                return(0);
        }

	/* Use first defined flash device (we probably have only one) */
#ifdef NVRAM_IN_FLASH
        nvram = (char *)(tgt_flashmap())->fl_map_base;

	/* Map. Deal with an entire sector even if we only use part of it */
        nvram += NVRAM_OFFS & ~(NVRAM_SECSIZE - 1);
	nvramsecbuf = (char *)malloc(NVRAM_SECSIZE);
	if(nvramsecbuf == 0) {
		printf("Warning! Unable to malloc nvrambuffer!\n");
		return(-1);
	}
        memcpy(nvramsecbuf, nvram, NVRAM_SECSIZE);
	nvrambuf = nvramsecbuf + (NVRAM_OFFS & (NVRAM_SECSIZE - 1));
#else
        nvramsecbuf = nvrambuf = nvram = (char *)malloc(512);
	nvram_get(nvram);
#endif

        ep = nvrambuf + 2;

	status = 0;
        while((*ep != '\0') && (ep <= nvrambuf + NVRAM_SIZE)) {
                np = name;
                sp = ep;

                while((*ep == *np) && (*ep != '=') && (*np != '\0')) {
                        ep++;
                        np++;
                }
                if((*np == '\0') && ((*ep == '\0') || (*ep == '='))) {
                        while(*ep++);
                        while(ep <= nvrambuf + NVRAM_SIZE) {
                                *sp++ = *ep++;
                        }
                        if(nvrambuf[2] == '\0') {
                                nvrambuf[3] = '\0';
                        }
                        cksum(nvrambuf, NVRAM_SIZE, 1);
#ifdef NVRAM_IN_FLASH
                        if(fl_erase_device(nvram, NVRAM_SECSIZE, FALSE)) {
                                status = -1;
				break;
                        }

                        if(fl_program_device(nvram, nvramsecbuf, NVRAM_SECSIZE, FALSE)) {
                                status = -1;
				break;
                        }
#else
			nvram_put(nvram);
#endif
			status = 1;
			break;
                }
                else if(*ep != '\0') {
                        while(*ep++ != '\0');
                }
        }

	free(nvramsecbuf);
        return(status);
}

int
tgt_setenv(char *name, char *value)
{
        char *ep;
        int envlen;
        char *nvrambuf;
        char *nvramsecbuf;
#ifdef NVRAM_IN_FLASH
        char *nvram;
#endif

	/* Non permanent vars. */
	if(strcmp(EXPERT, name) == 0) {
		return(1);
	}

        /* Calculate total env mem size requiered */
        envlen = strlen(name);
        if(envlen == 0) {
                return(0);
        }
        if(value != NULL) {
                envlen += strlen(value);
        }
        envlen += 2;    /* '=' + null byte */
        if(envlen > 255) {
                return(0);      /* Are you crazy!? */
        }

	/* Use first defined flash device (we probably have only one) */
#ifdef NVRAM_IN_FLASH
        nvram = (char *)(tgt_flashmap())->fl_map_base;

	/* Deal with an entire sector even if we only use part of it */
        nvram += NVRAM_OFFS & ~(NVRAM_SECSIZE - 1);
#endif

        /* If NVRAM is found to be uninitialized, reinit it. */
	if(nvram_invalid) {
		nvramsecbuf = (char *)malloc(NVRAM_SECSIZE);
		if(nvramsecbuf == 0) {
			printf("Warning! Unable to malloc nvrambuffer!\n");
			return(-1);
		}
#ifdef NVRAM_IN_FLASH
		memcpy(nvramsecbuf, nvram, NVRAM_SECSIZE);
#endif
		nvrambuf = nvramsecbuf + (NVRAM_OFFS & (NVRAM_SECSIZE - 1));
                memset(nvrambuf, -1, NVRAM_SIZE);
                nvrambuf[2] = '\0';
                nvrambuf[3] = '\0';
                cksum((void *)nvrambuf, NVRAM_SIZE, 1);
		printf("Warning! NVRAM checksum fail. Reset!\n");
#ifdef NVRAM_IN_FLASH
                if(fl_erase_device(nvram, NVRAM_SECSIZE, FALSE)) {
			printf("Error! Nvram erase failed!\n");
			free(nvramsecbuf);
                        return(-1);
                }
                if(fl_program_device(nvram, nvramsecbuf, NVRAM_SECSIZE, FALSE)) {
			printf("Error! Nvram init failed!\n");
			free(nvramsecbuf);
                        return(-1);
                }
#else
		nvram_put(nvramsecbuf);
#endif
                nvram_invalid = 0;
		free(nvramsecbuf);
        }

        /* Remove any current setting */
        tgt_unsetenv(name);

        /* Find end of evironment strings */
	nvramsecbuf = (char *)malloc(NVRAM_SECSIZE);
	if(nvramsecbuf == 0) {
		printf("Warning! Unable to malloc nvrambuffer!\n");
		return(-1);
	}
#ifndef NVRAM_IN_FLASH
	nvram_get(nvramsecbuf);
#else
        memcpy(nvramsecbuf, nvram, NVRAM_SECSIZE);
#endif
	nvrambuf = nvramsecbuf + (NVRAM_OFFS & (NVRAM_SECSIZE - 1));
	/* Etheraddr is special case to save space */
	if (strcmp("ethaddr", name) == 0) {
		char *s = value;
		int i;
		int32_t v;
		for(i = 0; i < 6; i++) {
			gethex(&v, s, 2);
			hwethadr[i] = v;
			s += 3;         /* Don't get to fancy here :-) */
		} 
	} else {
		ep = nvrambuf+2;
		if(*ep != '\0') {
			do {
				while(*ep++ != '\0');
			} while(*ep++ != '\0');
			ep--;
		}
		if(((int)ep + NVRAM_SIZE - (int)ep) < (envlen + 1)) {
			free(nvramsecbuf);
			return(0);      /* Bummer! */
		}

		/*
		 *  Special case heaptop must always be first since it
		 *  can change how memory allocation works.
		 */
		if(strcmp("heaptop", name) == 0) {

			bcopy(nvrambuf+2, nvrambuf+2 + envlen,
				 ep - nvrambuf+1);

			ep = nvrambuf+2;
			while(*name != '\0') {
				*ep++ = *name++;
			}
			if(value != NULL) {
				*ep++ = '=';
				while((*ep++ = *value++) != '\0');
			}
			else {
				*ep++ = '\0';
			}
		}
		else {
			while(*name != '\0') {
				*ep++ = *name++;
			}
			if(value != NULL) {
				*ep++ = '=';
				while((*ep++ = *value++) != '\0');
			}
			else {
				*ep++ = '\0';
			}
			*ep++ = '\0';   /* End of env strings */
		}
	}
        cksum(nvrambuf, NVRAM_SIZE, 1);

	bcopy(hwethadr, &nvramsecbuf[ETHER_OFFS], 6);
#ifdef NVRAM_IN_FLASH
        if(fl_erase_device(nvram, NVRAM_SECSIZE, FALSE)) {
		printf("Error! Nvram erase failed!\n");
		free(nvramsecbuf);
                return(0);
        }
        if(fl_program_device(nvram, nvramsecbuf, NVRAM_SECSIZE, FALSE)) {
		printf("Error! Nvram program failed!\n");
		free(nvramsecbuf);
                return(0);
        }
#else
	nvram_put(nvramsecbuf);
#endif
	free(nvramsecbuf);
        return(1);
}



/*
 *  Calculate checksum. If 'set' checksum is calculated and set.
 */
static int
cksum(void *p, size_t s, int set)
{
	u_int16_t sum = 0;
	u_int8_t *sp = p;
	int sz = s / 2;

	if(set) {
		*sp = 0;	/* Clear checksum */
		*(sp+1) = 0;	/* Clear checksum */
	}
	while(sz--) {
		sum += (*sp++) << 8;
		sum += *sp++;
	}
	if(set) {
		sum = -sum;
		*(u_int8_t *)p = sum >> 8;
		*((u_int8_t *)p+1) = sum;
	}
	return(sum);
}

#ifndef NVRAM_IN_FLASH

/*
 *  Read and write data into non volatile memory in clock chip.
 */
void
nvram_get(char *buffer)
{
	int i;
	for(i = 0; i < 114; i++) {
		linux_outb(i + RTC_NVRAM_BASE, RTC_INDEX_REG);	/* Address */
		buffer[i] = linux_inb(RTC_DATA_REG);
	}
}

void
nvram_put(char *buffer)
{
	int i;
	for(i = 0; i < 114; i++) {
		linux_outb(i+RTC_NVRAM_BASE, RTC_INDEX_REG);	/* Address */
		linux_outb(buffer[i],RTC_DATA_REG);
	}
}

#endif

/*
 *  Simple display function to display a 4 char string or code.
 *  Called during startup to display progress on any feasible
 *  display before any serial port have been initialized.
 */
void
tgt_display(char *msg, int x)
{
	/* Have simple serial port driver */
	tgt_putchar(msg[0]);
	tgt_putchar(msg[1]);
	tgt_putchar(msg[2]);
	tgt_putchar(msg[3]);
	tgt_putchar('\r');
	tgt_putchar('\n');
}

void
clrhndlrs()
{
}

int
tgt_getmachtype()
{
	return(md_cputype());
}

/*
 *  Create stubs if network is not compiled in
 */
#ifdef INET
void
tgt_netpoll()
{
	splx(splhigh());
}

#else
extern void longjmp(label_t *, int);
void gsignal(label_t *jb, int sig);
void
gsignal(label_t *jb, int sig)
{
	if(jb != NULL) {
		longjmp(jb, 1);
	}
};

int	netopen (const char *, int);
int	netread (int, void *, int);
int	netwrite (int, const void *, int);
long	netlseek (int, long, int);
int	netioctl (int, int, void *);
int	netclose (int);
int netopen(const char *p, int i)	{ return -1;}
int netread(int i, void *p, int j)	{ return -1;}
int netwrite(int i, const void *p, int j)	{ return -1;}
int netclose(int i)	{ return -1;}
long int netlseek(int i, long j, int k)	{ return -1;}
int netioctl(int j, int i, void *p)	{ return -1;}
void tgt_netpoll()	{};

#endif /*INET*/

#define SPINSZ		0x800000
#define DEFTESTS	7
#define MOD_SZ		20
#define BAILOUT		if (bail) goto skip_test;
#define BAILR		if (bail) return;

/* memspeed operations */
#define MS_BCOPY	1
#define MS_COPY		2
#define MS_WRITE	3
#define MS_READ		4
#include "mycmd.c"
